# Scribunto Modules

These are some Scribunto modules I've developed for the [Rain World Wiki] which I've chosen to make more easily reusable.

While an effort will be made to support older versions if requested, my main effort will be put toward making these compatible with the versions of MediaWiki and Scribunto used on Miraheze, which will likely be close to the latest version. You can see what those versions are specifically on [meta:Special:Version].

All of them are licensed CC-BY-SA 4.0.

[Rain World Wiki]: https://rainworld.miraheze.org
[meta:Special:Version]: https://meta.miraheze.org/wiki/Special:Version
