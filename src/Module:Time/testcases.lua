--- Module:Time/testcases
--- by User:LtDk, licensed CC-BY-SA 4.0
--- Tests for Time module.

local this = require('Module:ScribuntoUnit'):new()
local Time = require('Module:Time')

function this:testIsTimeEmpty()
    self:assertTrue(
        Time.isTime(Time.new())
    )
end

function this:testIsTimeNonempty()
    self:assertTrue(
        Time.isTime(Time.new(4000, 4000, 4000))
    )
end

function this:test0()
    self:assertEquals(
        Time.new(0, 0, 0),
        Time.new()
    )
end

function this:test40s()
    self:assertEquals(
        Time.new(0, 0, 40),
        Time.new(40)
    )
end

function this:test400s()
    self:assertEquals(
        Time.new(0, 6, 40),
        Time.new(400)
    )
end

function this:test4000s()
    self:assertEquals(
        Time.new(1, 6, 40),
        Time.new(4000)
    )
end

function this:test40m40s()
    self:assertEquals(
        Time.new(0, 40, 40),
        Time.new(40, 40)
    )
end

function this:test40m400s()
    self:assertEquals(
        Time.new(0, 46, 40),
        Time.new(40, 400)
    )
end

function this:test40m4000s()
    self:assertEquals(
        Time.new(1, 46, 40),
        Time.new(40, 4000)
    )
end

function this:test40h40m40s()
    self:assertEquals(
        Time.new(40, 40, 40),
        Time.new(40, 40, 40)
    )
end

function this:test40h40m400s()
    self:assertEquals(
        Time.new(40, 46, 40),
        Time.new(40, 40, 400)
    )
end

function this:test40h40m4000s()
    self:assertEquals(
        Time.new(41, 46, 40),
        Time.new(40, 40, 4000)
    )
end

function this:test40h400m40s()
    self:assertEquals(
        Time.new(46, 40, 40),
        Time.new(40, 400, 40)
    )
end

function this:test40h400m400s()
    self:assertEquals(
        Time.new(46, 46, 40),
        Time.new(40, 400, 400)
    )
end

function this:test40h400m4000s()
    self:assertEquals(
        Time.new(47, 46, 40),
        Time.new(40, 400, 4000)
    )
end

function this:testParseEmpty()
    self:assertThrows(
        function ()
            local _ = Time.parse('')
        end,
        '"" was not an integer in ""'
    )
end

function this:testParse400s()
    self:assertEquals(
        Time.new(400),
        Time.parse('400')
    )
end

function this:testParse400m400s()
    self:assertEquals(
        Time.new(400, 400),
        Time.parse('400:400')
    )
end

function this:testParse400h400m400s()
    self:assertEquals(
        Time.new(400, 400, 400),
        Time.parse('400:400:400')
    )
end

function this:testParseTooMany()
    self:assertThrows(
        function ()
            local _ = Time.parse('400:400:400:400')
        end,
        '"400:400" was not an integer in "400:400:400:400"'
    )
end

return this
