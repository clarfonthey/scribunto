--- @meta

--- @class ExpandArgs
--- @field template string|mw.title
--- @field args { [string|number]: string }

--- @class mw.frame
--- @field args            { [string|number]: string }
--- @field getParent       fun(mw.frame): mw.frame
--- @field getCurrentTItle fun(mw.frame): mw.title
--- @field expandTemplate  fun(mw.frame, ExpandArgs): string

--- @class mw.title
