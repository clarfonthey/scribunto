--- Module:StringSet/testcases
--- by User:LtDk, licensed CC-BY-SA 4.0
--- Tests for StringSet module.

local this = require('Module:ScribuntoUnit'):new()
local StringSet = require('Module:StringSet')

function this:testIsStringSet()
    local ss = StringSet.new()
    self:assertTrue(StringSet.isStringSet(ss))
end

function this:testEmptyLen()
    local ss = StringSet.new()
    self:assertEquals(0, ss:len())
end

function this:testNonEmptyLen()
    local ss = StringSet.new { '1', '2', '3', '4' }
    self:assertEquals(4, ss:len())
end

function this:testRemove()
    local act = StringSet.new { '1', '2', '3', '4' }
    act:removeAll { '2', '3' }
    local exp = StringSet.new { '1', '4' }
    self:assertEquals(exp, act)
end

function this:testSubset()
    local super = StringSet.new { '1', '2', '3', '4' }
    local sub = StringSet.new { '1', '3' }
    self:assertTrue(sub <= super)
end

function this:testKeysConvertedToString()
    --- @diagnostic disable-next-line
    local lhs = StringSet.new { 1, 2, 3, 4 }
    local rhs = StringSet.new { '1', '2', '3', '4' }
    self:assertEquals(rhs, lhs)
end

function this:testToString()
    local ss = StringSet.new { '4', '2', '3', '1' }
    self:assertEquals('{"1", "2", "3", "4"}', tostring(ss))
end

return this
