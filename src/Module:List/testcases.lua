--- Module:List/testcases
--- by User:LtDk, licensed CC-BY-SA 4.0
--- Tests for List module.

local this = require('Module:ScribuntoUnit'):new()
local List = require('Module:List')
local Item = List.Item

function this:testIsItem()
    local item = Item.new('item')
    self:assertTrue(List.isItem(item))
end

function this:testItemFormatBefore()
    local item = Item.new('item', 'weight')
    self:assertEquals('[weight]item', item:formatBefore('[', ']'))
end

function this:testItemFormatBeforeNoWeight()
    local item = Item.new('item')
    self:assertEquals('item', item:formatBefore('[', ']'))
end

function this:testItemFormatAfter()
    local item = Item.new('item', 'weight')
    self:assertEquals('item[weight]', item:formatAfter('[', ']'))
end

function this:testItemFormatAfterNoWeight()
    local item = Item.new('item')
    self:assertEquals('item', item:formatAfter('[', ']'))
end

function this:testItemDefaultCompareWeight()
    local lhs = Item.new('z', 1)
    local rhs = Item.new('a', 2)
    self:assertTrue(lhs < rhs)
end

function this:testItemCompareValue()
    local lhs = Item.new('z', 1)
    local rhs = Item.new('a', 2)
    rawset(lhs, '__byvalue', true)
    rawset(rhs, '__byvalue', true)
    self:assertTrue(rhs < lhs)
end

function this:testItemCompareValueNil()
    local lhs = Item.new('z', 1)
    local rhs = Item.new('a', 2)
    rawset(lhs, '__byvalue', true)
    self:assertTrue(rhs < lhs)
end

function this:testItemCompareNilValue()
    local lhs = Item.new('z', 1)
    local rhs = Item.new('a', 2)
    rawset(rhs, '__byvalue', true)
    self:assertTrue(rhs < lhs)
end

function this:testItemCompareWeight()
    local lhs = Item.new('z', 1)
    local rhs = Item.new('a', 2)
    rawset(lhs, '__byvalue', false)
    rawset(rhs, '__byvalue', false)
    self:assertTrue(lhs < rhs)
end

function this:testItemCompareWeightNil()
    local lhs = Item.new('z', 1)
    local rhs = Item.new('a', 2)
    rawset(lhs, '__byvalue', false)
    self:assertTrue(lhs < rhs)
end

function this:testItemCompareNilWeight()
    local lhs = Item.new('z', 1)
    local rhs = Item.new('a', 2)
    rawset(rhs, '__byvalue', false)
    self:assertTrue(lhs < rhs)
end

function this:testItemCompareValueWeight()
    local lhs = Item.new('z', 1)
    local rhs = Item.new('a', 2)
    rawset(lhs, '__byvalue', true)
    rawset(rhs, '__byvalue', false)
    self:assertThrows(function ()
        local _ = lhs < rhs
    end)
end

function this:testItemCompareWeightValue()
    local lhs = Item.new('z', 1)
    local rhs = Item.new('a', 2)
    rawset(lhs, '__byvalue', false)
    rawset(rhs, '__byvalue', true)
    self:assertThrows(function ()
        local _ = lhs < rhs
    end)
end

function this:testItemCompareSortNil()
    local lhs = Item.new('z', 1)
    local rhs = Item.new('a')
    self:assertTrue(lhs < rhs)
end

function this:testIsList()
    local l = List.new()
    self:assertTrue(List.isList(l))
end

function this:testEmptyLen()
    local l = List.new()
    self:assertEquals(0, l:len())
end

function this:testNonEmptyLen()
    local l = List.new { 'one', 'two', 'three', 'four' }
    self:assertEquals(4, l:len())
end

function this:testPairs()
    local l = List.new { { 'one', 4 }, { 'two', 3 }, { 'three', 2 }, { 'four', 1 } }
    local l2 = {}
    local prev = 0
    for idx, item in pairs(l) do
        self:assertTrue(prev < idx, string.format('%d >= %d', prev, idx))
        table.insert(l2, item)
    end
    self:assertDeepEquals(
        {
            Item.new('one', 4),
            Item.new('two', 3),
            Item.new('three', 2),
            Item.new('four', 1),
        },
        l2
    )
end

local creatures = {
    Lizard = 3,
    Guardian = 4,
    Eggbug = 5,
}
local CreatureMeta = {
    __tostring = function (t)
        return t.name
    end,
    __eq = function (lhs, rhs)
        return lhs.id == rhs.id
    end,
    __lt = function (lhs, rhs)
        return lhs.id < rhs.id
    end,
    __le = function (lhs, rhs)
        return lhs.id <= rhs.id
    end,
}
local function validateCreature(val)
    local id = creatures[val]
    if id ~= nil then
        local obj = {
            id = id,
            name = val,
        }
        setmetatable(obj, CreatureMeta)
        return obj
    end
end

function this:testSimpleCreatures()
    local l = List.new(
        { 'Guardian', { 'Lizard', '10' }, { 'Eggbug', '5' } },
        validateCreature,
        List.numericWeight
    )
    local expect = {
        Item.new(validateCreature('Guardian')),
        Item.new(validateCreature('Lizard'), 10),
        Item.new(validateCreature('Eggbug'), 5),
    }
    local actual = {}
    for _, item in ipairs(l) do
        table.insert(actual, item)
    end
    self:assertDeepEquals(expect, actual)
end

function this:testSimpleCreaturesByValue()
    local l = List.byValue(
        { 'Guardian', { 'Lizard', '10' }, { 'Eggbug', '5' } },
        validateCreature,
        List.numericWeight
    )
    l:sort()
    local expect = {
        Item.new(validateCreature('Lizard'), 10),
        Item.new(validateCreature('Guardian')),
        Item.new(validateCreature('Eggbug'), 5),
    }
    local actual = {}
    for _, item in ipairs(l) do
        table.insert(actual, item)
    end
    self:assertDeepEquals(expect, actual)
end

function this:testSimpleCreaturesByWeight()
    local l = List.byWeight(
        { 'Guardian', { 'Lizard', '10' }, { 'Eggbug', '5' } },
        validateCreature,
        List.numericWeight
    )
    l:sort()
    local expect = {
        Item.new(validateCreature('Eggbug'), 5),
        Item.new(validateCreature('Lizard'), 10),
        Item.new(validateCreature('Guardian')),
    }
    local actual = {}
    for _, item in ipairs(l) do
        table.insert(actual, item)
    end
    self:assertDeepEquals(expect, actual)
end

function this:testNestedCreatures()
    local l = List.byValueParsed(
        'Guardian (Lizard (10), Eggbug (5)), Lizard (2)',
        validateCreature,
        List.nested(validateCreature, List.numericWeight)
    )
    self:assertEquals('Guardian[Lizard[10];Eggbug[5]];Lizard[2]', l:formatAfter('[', ']', ';'))
end

function this:testLookup()
    local l = List.parse('Good (1), Good (2), Good, Bad (3)'):toLookup()
    self:assertTrue(l['Good'][List.NilWeight])
    self:assertTrue(l['Good']['1'])
    self:assertTrue(l['Good']['2'])
    self:assertTrue(l['Bad']['3'])
    self:assertFalse(l['Bad'][List.NilWeight])
    self:assertFalse(l['Ugly'][List.NilWeight])
end

function this:testLookupHas()
    local l = List.parse('Good (1), Good (2), Good, Bad (3)'):toLookup()
    self:assertTrue(List.lookupHas(l, 'Good'))
    self:assertTrue(List.lookupHas(l, 'Bad'))
    self:assertFalse(List.lookupHas(l, 'Ugly'))
end

return this
