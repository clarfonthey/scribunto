--- Module:FileLink/testcases
--- by User:LtDk, licensed CC-BY-SA 4.0
--- Tests for FileLink module.

local this = require('Module:ScribuntoUnit'):new()
local FileLink = require('Module:FileLink')

function this:testSimple()
    self:assertEquals(
        '[[File:Image.png]]',
        tostring(FileLink.new('Image.png'))
    )
end

function this:testComplicated()
    local args = {
        'Image.png',
        'border',
        'frame',
        'framed',
        'frameless',
        '24',
        'x24',
        '48x48',
        'cool',
        thumbtime = '1:2',
        alt = 'neat',
    }
    self:assertEquals(
        '[[File:Image.png|border|frameless|48x48px|alt=neat|thumbtime=01:02|cool]]',
        tostring(FileLink.fromArgs(args))
    )
end

return this
