--- Module:Unicode
--- by User:LtDk, licensed CC-BY-SA 4.0
--- Code specifically for handling aspects of Unicode that templates can't.

local this = {}

--- Map of open to close bracket characters according to UCD BidiBrackets file.
--- @enum (key) Open
--- @enum Close
--- @type { [Open]: Close }
local open_close = {
    [0x0028] = 0x0029, -- LEFT PARENTHESIS
    [0x005B] = 0x005D, -- LEFT SQUARE BRACKET
    [0x007B] = 0x007D, -- LEFT CURLY BRACKET
    [0x0F3A] = 0x0F3B, -- TIBETAN MARK GUG RTAGS GYON
    [0x0F3C] = 0x0F3D, -- TIBETAN MARK ANG KHANG GYON
    [0x169B] = 0x169C, -- OGHAM FEATHER MARK
    [0x2045] = 0x2046, -- LEFT SQUARE BRACKET WITH QUILL
    [0x207D] = 0x207E, -- SUPERSCRIPT LEFT PARENTHESIS
    [0x208D] = 0x208E, -- SUBSCRIPT LEFT PARENTHESIS
    [0x2308] = 0x2309, -- LEFT CEILING
    [0x230A] = 0x230B, -- LEFT FLOOR
    [0x2329] = 0x232A, -- LEFT-POINTING ANGLE BRACKET
    [0x2768] = 0x2769, -- MEDIUM LEFT PARENTHESIS ORNAMENT
    [0x276A] = 0x276B, -- MEDIUM FLATTENED LEFT PARENTHESIS ORNAMENT
    [0x276C] = 0x276D, -- MEDIUM LEFT-POINTING ANGLE BRACKET ORNAMENT
    [0x276E] = 0x276F, -- HEAVY LEFT-POINTING ANGLE QUOTATION MARK ORNAMENT
    [0x2770] = 0x2771, -- HEAVY LEFT-POINTING ANGLE BRACKET ORNAMENT
    [0x2772] = 0x2773, -- LIGHT LEFT TORTOISE SHELL BRACKET ORNAMENT
    [0x2774] = 0x2775, -- MEDIUM LEFT CURLY BRACKET ORNAMENT
    [0x27C5] = 0x27C6, -- LEFT S-SHAPED BAG DELIMITER
    [0x27E6] = 0x27E7, -- MATHEMATICAL LEFT WHITE SQUARE BRACKET
    [0x27E8] = 0x27E9, -- MATHEMATICAL LEFT ANGLE BRACKET
    [0x27EA] = 0x27EB, -- MATHEMATICAL LEFT DOUBLE ANGLE BRACKET
    [0x27EC] = 0x27ED, -- MATHEMATICAL LEFT WHITE TORTOISE SHELL BRACKET
    [0x27EE] = 0x27EF, -- MATHEMATICAL LEFT FLATTENED PARENTHESIS
    [0x2983] = 0x2984, -- LEFT WHITE CURLY BRACKET
    [0x2985] = 0x2986, -- LEFT WHITE PARENTHESIS
    [0x2987] = 0x2988, -- Z NOTATION LEFT IMAGE BRACKET
    [0x2989] = 0x298A, -- Z NOTATION LEFT BINDING BRACKET
    [0x298B] = 0x298C, -- LEFT SQUARE BRACKET WITH UNDERBAR
    [0x298D] = 0x2990, -- LEFT SQUARE BRACKET WITH TICK IN TOP CORNER
    [0x298F] = 0x298E, -- LEFT SQUARE BRACKET WITH TICK IN BOTTOM CORNER
    [0x2991] = 0x2992, -- LEFT ANGLE BRACKET WITH DOT
    [0x2993] = 0x2994, -- LEFT ARC LESS-THAN BRACKET
    [0x2995] = 0x2996, -- DOUBLE LEFT ARC GREATER-THAN BRACKET
    [0x2997] = 0x2998, -- LEFT BLACK TORTOISE SHELL BRACKET
    [0x29D8] = 0x29D9, -- LEFT WIGGLY FENCE
    [0x29DA] = 0x29DB, -- LEFT DOUBLE WIGGLY FENCE
    [0x29FC] = 0x29FD, -- LEFT-POINTING CURVED ANGLE BRACKET
    [0x2E22] = 0x2E23, -- TOP LEFT HALF BRACKET
    [0x2E24] = 0x2E25, -- BOTTOM LEFT HALF BRACKET
    [0x2E26] = 0x2E27, -- LEFT SIDEWAYS U BRACKET
    [0x2E28] = 0x2E29, -- LEFT DOUBLE PARENTHESIS
    [0x2E55] = 0x2E56, -- LEFT SQUARE BRACKET WITH STROKE
    [0x2E57] = 0x2E58, -- LEFT SQUARE BRACKET WITH DOUBLE STROKE
    [0x2E59] = 0x2E5A, -- TOP HALF LEFT PARENTHESIS
    [0x2E5B] = 0x2E5C, -- BOTTOM HALF LEFT PARENTHESIS
    [0x3008] = 0x3009, -- LEFT ANGLE BRACKET
    [0x300A] = 0x300B, -- LEFT DOUBLE ANGLE BRACKET
    [0x300C] = 0x300D, -- LEFT CORNER BRACKET
    [0x300E] = 0x300F, -- LEFT WHITE CORNER BRACKET
    [0x3010] = 0x3011, -- LEFT BLACK LENTICULAR BRACKET
    [0x3014] = 0x3015, -- LEFT TORTOISE SHELL BRACKET
    [0x3016] = 0x3017, -- LEFT WHITE LENTICULAR BRACKET
    [0x3018] = 0x3019, -- LEFT WHITE TORTOISE SHELL BRACKET
    [0x301A] = 0x301B, -- LEFT WHITE SQUARE BRACKET
    [0xFE59] = 0xFE5A, -- SMALL LEFT PARENTHESIS
    [0xFE5B] = 0xFE5C, -- SMALL LEFT CURLY BRACKET
    [0xFE5D] = 0xFE5E, -- SMALL LEFT TORTOISE SHELL BRACKET
    [0xFF08] = 0xFF09, -- FULLWIDTH LEFT PARENTHESIS
    [0xFF3B] = 0xFF3D, -- FULLWIDTH LEFT SQUARE BRACKET
    [0xFF5B] = 0xFF5D, -- FULLWIDTH LEFT CURLY BRACKET
    [0xFF5F] = 0xFF60, -- FULLWIDTH LEFT WHITE PARENTHESIS
    [0xFF62] = 0xFF63, -- HALFWIDTH LEFT CORNER BRACKET
}

--- Map of close to open bracket characters according to UCD BidiBrackets file.
--- @type { [Close]: Open }
local close_open = {
    [0x0029] = 0x0028, -- RIGHT PARENTHESIS
    [0x005D] = 0x005B, -- RIGHT SQUARE BRACKET
    [0x007D] = 0x007B, -- RIGHT CURLY BRACKET
    [0x0F3B] = 0x0F3A, -- TIBETAN MARK GUG RTAGS GYAS
    [0x0F3D] = 0x0F3C, -- TIBETAN MARK ANG KHANG GYAS
    [0x169C] = 0x169B, -- OGHAM REVERSED FEATHER MARK
    [0x2046] = 0x2045, -- RIGHT SQUARE BRACKET WITH QUILL
    [0x207E] = 0x207D, -- SUPERSCRIPT RIGHT PARENTHESIS
    [0x208E] = 0x208D, -- SUBSCRIPT RIGHT PARENTHESIS
    [0x2309] = 0x2308, -- RIGHT CEILING
    [0x230B] = 0x230A, -- RIGHT FLOOR
    [0x232A] = 0x2329, -- RIGHT-POINTING ANGLE BRACKET
    [0x2769] = 0x2768, -- MEDIUM RIGHT PARENTHESIS ORNAMENT
    [0x276B] = 0x276A, -- MEDIUM FLATTENED RIGHT PARENTHESIS ORNAMENT
    [0x276D] = 0x276C, -- MEDIUM RIGHT-POINTING ANGLE BRACKET ORNAMENT
    [0x276F] = 0x276E, -- HEAVY RIGHT-POINTING ANGLE QUOTATION MARK ORNAMENT
    [0x2771] = 0x2770, -- HEAVY RIGHT-POINTING ANGLE BRACKET ORNAMENT
    [0x2773] = 0x2772, -- LIGHT RIGHT TORTOISE SHELL BRACKET ORNAMENT
    [0x2775] = 0x2774, -- MEDIUM RIGHT CURLY BRACKET ORNAMENT
    [0x27C6] = 0x27C5, -- RIGHT S-SHAPED BAG DELIMITER
    [0x27E7] = 0x27E6, -- MATHEMATICAL RIGHT WHITE SQUARE BRACKET
    [0x27E9] = 0x27E8, -- MATHEMATICAL RIGHT ANGLE BRACKET
    [0x27EB] = 0x27EA, -- MATHEMATICAL RIGHT DOUBLE ANGLE BRACKET
    [0x27ED] = 0x27EC, -- MATHEMATICAL RIGHT WHITE TORTOISE SHELL BRACKET
    [0x27EF] = 0x27EE, -- MATHEMATICAL RIGHT FLATTENED PARENTHESIS
    [0x2984] = 0x2983, -- RIGHT WHITE CURLY BRACKET
    [0x2986] = 0x2985, -- RIGHT WHITE PARENTHESIS
    [0x2988] = 0x2987, -- Z NOTATION RIGHT IMAGE BRACKET
    [0x298A] = 0x2989, -- Z NOTATION RIGHT BINDING BRACKET
    [0x298C] = 0x298B, -- RIGHT SQUARE BRACKET WITH UNDERBAR
    [0x298E] = 0x298F, -- RIGHT SQUARE BRACKET WITH TICK IN BOTTOM CORNER
    [0x2990] = 0x298D, -- RIGHT SQUARE BRACKET WITH TICK IN TOP CORNER
    [0x2992] = 0x2991, -- RIGHT ANGLE BRACKET WITH DOT
    [0x2994] = 0x2993, -- RIGHT ARC GREATER-THAN BRACKET
    [0x2996] = 0x2995, -- DOUBLE RIGHT ARC LESS-THAN BRACKET
    [0x2998] = 0x2997, -- RIGHT BLACK TORTOISE SHELL BRACKET
    [0x29D9] = 0x29D8, -- RIGHT WIGGLY FENCE
    [0x29DB] = 0x29DA, -- RIGHT DOUBLE WIGGLY FENCE
    [0x29FD] = 0x29FC, -- RIGHT-POINTING CURVED ANGLE BRACKET
    [0x2E23] = 0x2E22, -- TOP RIGHT HALF BRACKET
    [0x2E25] = 0x2E24, -- BOTTOM RIGHT HALF BRACKET
    [0x2E27] = 0x2E26, -- RIGHT SIDEWAYS U BRACKET
    [0x2E29] = 0x2E28, -- RIGHT DOUBLE PARENTHESIS
    [0x2E56] = 0x2E55, -- RIGHT SQUARE BRACKET WITH STROKE
    [0x2E58] = 0x2E57, -- RIGHT SQUARE BRACKET WITH DOUBLE STROKE
    [0x2E5A] = 0x2E59, -- TOP HALF RIGHT PARENTHESIS
    [0x2E5C] = 0x2E5B, -- BOTTOM HALF RIGHT PARENTHESIS
    [0x3009] = 0x3008, -- RIGHT ANGLE BRACKET
    [0x300B] = 0x300A, -- RIGHT DOUBLE ANGLE BRACKET
    [0x300D] = 0x300C, -- RIGHT CORNER BRACKET
    [0x300F] = 0x300E, -- RIGHT WHITE CORNER BRACKET
    [0x3011] = 0x3010, -- RIGHT BLACK LENTICULAR BRACKET
    [0x3015] = 0x3014, -- RIGHT TORTOISE SHELL BRACKET
    [0x3017] = 0x3016, -- RIGHT WHITE LENTICULAR BRACKET
    [0x3019] = 0x3018, -- RIGHT WHITE TORTOISE SHELL BRACKET
    [0x301B] = 0x301A, -- RIGHT WHITE SQUARE BRACKET
    [0xFE5A] = 0xFE59, -- SMALL RIGHT PARENTHESIS
    [0xFE5C] = 0xFE5B, -- SMALL RIGHT CURLY BRACKET
    [0xFE5E] = 0xFE5D, -- SMALL RIGHT TORTOISE SHELL BRACKET
    [0xFF09] = 0xFF08, -- FULLWIDTH RIGHT PARENTHESIS
    [0xFF3D] = 0xFF3B, -- FULLWIDTH RIGHT SQUARE BRACKET
    [0xFF5D] = 0xFF5B, -- FULLWIDTH RIGHT CURLY BRACKET
    [0xFF60] = 0xFF5F, -- FULLWIDTH RIGHT WHITE PARENTHESIS
    [0xFF63] = 0xFF62, -- HALFWIDTH RIGHT CORNER BRACKET
}

--- Pattern to find any opening bracket.
local open_pattern_any = '['

--- Pattern to find any closing bracket.
local close_pattern_any = '['

--- Pattern to match balanced brackets based upon the open bracket.
--- @type { [Open]: string }
local open_patterns = {}

--- Pattern to match balanced brackets based upon the close bracket.
--- @type { [Close]: string }
local close_patterns = {}

for open, close in pairs(open_close) do
    local open_char = mw.ustring.char(open)
    local close_char = mw.ustring.char(close)
    local close_char_escape = ''
    if close_char == ']' then
        close_char_escape = '%'
    end

    open_pattern_any = open_pattern_any .. open_char
    open_patterns[open] = '%b' .. open_char .. close_char

    close_pattern_any = close_pattern_any .. close_char_escape .. close_char
    close_patterns[close] = '%b' .. close_char .. open_char
end

open_pattern_any = open_pattern_any .. ']'
close_pattern_any = close_pattern_any .. ']'


--- Set of comma characters used for comma-separator in MediaWiki. (not part of Unicode)
--- @enum (key) Comma
--- @type { [Comma]: true }
local commas = {
    [0x002C] = true, -- COMMA
    [0X060C] = true, -- ARABIC COMMA
    [0X1363] = true, -- ETHIOPIC COMMA
    [0X3001] = true, -- IDEOGRAPHIC COMMA
    [0XA828] = true, -- SYLOTI NAGRI POETRY MARK-1
}

--- Pattern to find any comma.
local comma_pattern = '['
for comma, _ in pairs(commas) do
    local char = mw.ustring.char(comma)
    comma_pattern = comma_pattern .. char
end

comma_pattern = comma_pattern .. ']'

--- Patterns for finding various characters.
--- @class Patterns
--- @field comma         string  pattern for finding commas
--- @field open_bracket  string  pattern for finding open brackets
--- @field close_bracket string  pattern for finding close brackets
this.patterns = {
    comma = comma_pattern,
    open_bracket = open_pattern_any,
    close_bracket = close_pattern_any,
}

--- Map of bracket code points to their pairs.
--- @class BracketPairs
--- @field open  { [Open]: Close }  map of open codepoints to close codepoints
--- @field close { [Close]: Open }  map of close codepoints to open codepoints
this.bracketPairs = {
    open = open_close,
    close = close_open,
}

--- Finds the first pair of matched brackets in a string.
--- @param s    string    the string to search
--- @param init number?   starting index of search in codepoints
--- @param rev  boolean?  if set to true, expects the brackets to appear in reversed order
--- @return number?  starting index of bracket span in codepoints
--- @return number?  ending index of bracket span in codepoints
function this.findBrackets(s, init, rev)
    local search, match
    if rev then
        search = close_pattern_any
        match = close_patterns
    else
        search = open_pattern_any
        match = open_patterns
    end

    local i, j = mw.ustring.find(s, search, init)
    if i == nil then
        return
    end
    local balanced = match[mw.ustring.codepoint(s, i, j)]

    return mw.ustring.find(s, balanced, i)
end

--- Splits text with brackets into its component parts.
--- @param s     string    string to split
--- @param init  number?   starting index of split, in codepoints
--- @param rev   boolean?  if true, expects the brackets to occur in reversed order
--- @return string  text before brackets
--- @return string  open bracket, or close bracket if rev == true
--- @return string  text inside brackets
--- @return string  close bracket, or open bracket if rev == true
--- @return string  text after brackets
function this.splitBrackets(s, init, rev)
    local i, j = this.findBrackets(s, init, rev)
    if i == nil then
        return mw.ustring.sub(s, init), '', '', '', ''
    end

    local before = mw.ustring.sub(s, init, i - 1)
    local open = mw.ustring.sub(s, i, i)
    local inside = mw.ustring.sub(s, i + 1, j - 1)
    local close = mw.ustring.sub(s, j, j)
    local after = mw.ustring.sub(s, j + 1)

    -- trim whitespace around brackets
    before = mw.ustring.gsub(before, '%s+$', '')
    after = mw.ustring.gsub(after, '^%s+', '')

    return before, open, inside, close, after
end

--- Version of splitBrackets that prefers brackets on one side of the text.
--- @param s     string    string to split
--- @param init  number?   starting index of split, in codepoints
--- @param rev   boolean?  if true, expects the brackets to occur in reversed order
--- @param alt   boolean?  if true, expects the brackets to occur before the unbracketed text
--- @return string   text outside brackets
--- @return string   text inside brackets
--- @return string   open bracket, or close bracket if rev == true
--- @return string   close bracket, or open bracket if rev == true
--- @return string   extraneous text on wrong side of brackets
function this.tryParseBrackets(s, init, rev, alt)
    local before, open, inside, close, after = this.splitBrackets(s, init, rev)
    if alt then
        return after, inside, open, close, before
    else
        return before, inside, open, close, after
    end
end

--- Version of tryParseBrackets that returns nil if brackets are ordered incorrectly.
--- @param s     string    string to split
--- @param init  number?   starting index of split, in codepoints
--- @param rev   boolean?  if true, expects the brackets to occur in reversed order
--- @param alt   boolean?  if true, expects the brackets to occur before the unbracketed text
--- @return string?  text outside brackets
--- @return string?  text inside brackets
--- @return string?  open bracket, or close bracket if rev == true
--- @return string?  close bracket, or open bracket if rev == true
function this.parseBrackets(s, init, rev, alt)
    local outside, inside, open, close, extra = this.tryParseBrackets(s, init, rev, alt)
    if extra == '' then
        return outside, inside, open, close
    else
        return nil, nil, nil, nil
    end
end

--- Finds the next occurrence of a pattern outside of any brackets.
--- May behave weirdly if the pattern contains brackets itself.
--- @param s    string    the string to search
--- @param pat  string    the ustring pattern to split
--- @param init number?   starting index of search, in codepoints
--- @param rev  boolean?  if set to true, expects the brackets to appear in reversed order
--- @return number?  starting index of match span in codepoints
--- @return number?  ending index of match span in codepoints
function this.findOutsideBrackets(s, pat, init, rev)
    local i, j = mw.ustring.find(s, pat, init)
    repeat
        if i == nil then
            return
        end
        local start, stop = this.findBrackets(s, init, rev)
        if start == nil or stop < i or start > i then
            break
        end
        init = stop + 1
        i, j = mw.ustring.find(s, pat, init)
    until true
    return i, j
end

--- Finds the next comma in a string after the given index.
--- @param s    string   the string to search
--- @param init number?  starting index of search, in codepoints
--- @return number?  index after next comma; negative if no more commas
--- @return string?  string before comma
function this.nextComma(s, init, rev)
    if init ~= nil and init < 0 then
        return nil
    end

    -- find next comma
    local comma = this.findOutsideBrackets(s, comma_pattern, init, rev)
    local stop, after
    if comma ~= nil then
        stop = comma - 1
        after = comma + 1
    end

    -- trim surrounding space
    s = mw.ustring.sub(s, init, stop)
    s = mw.ustring.gsub(s, '^%s*(.-)%s*$', '%1')
    return after or -1, s
end

--- Splits a list on commas. Trims whitespace around items.
--- @param s    string   the string to split
--- @param init number?  starting index of split, in codepoints
--- @param rev  boolean?  if set to true, expects the brackets to appear in reversed order
--- @return fun(string, number?): number?, string?  nextComma function
--- @return string                                  s
--- @return number?                                 init
function this.splitCommas(s, init, rev)
    return function (s, init)
        return this.nextComma(s, init, rev)
    end, s, init
end

return this
