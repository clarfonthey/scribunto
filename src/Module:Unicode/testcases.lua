--- Module:Unicode/testcases
--- by User:LtDk, licensed CC-BY-SA 4:0
--- Tests for Unicode module.

local this = require('Module:ScribuntoUnit'):new()
local Unicode = require('Module:Unicode')

function this:testForwardNone()
    this:assertDeepEquals(
        {},
        { Unicode.findBrackets('hello, world!', 1, false) }
    )
end

function this:testBackwardNone()
    this:assertDeepEquals(
        {},
        { Unicode.findBrackets('hello, world!', 1, true) }
    )
end

function this:testForwardForward()
    this:assertDeepEquals(
        { 8, 14 },
        { Unicode.findBrackets('hello, （world）!', 1, false) }
    )
end

function this:testBackwardBackward()
    this:assertDeepEquals(
        { 8, 14 },
        { Unicode.findBrackets('hello, ）world（!', 1, true) }
    )
end

function this:testForwardBackward()
    this:assertDeepEquals(
        {},
        { Unicode.findBrackets('hello, ）world（!', 1, false) }
    )
end

function this:testBackwardForward()
    this:assertDeepEquals(
        {},
        { Unicode.findBrackets('hello, （world）!', 1, true) }
    )
end

function this:testForwardForwardSplitBrackets()
    this:assertDeepEquals(
        { ' hello', '[', ' world [ my beloved ] ', ']', '! ' },
        { Unicode.splitBrackets(' hello [ world [ my beloved ] ] ! ', 1, false) }
    )
end

function this:testForwardBackwardSplitBrackets()
    this:assertDeepEquals(
        { ' hello ] world ] my beloved [ [ ! ', '', '', '', '' },
        { Unicode.splitBrackets(' hello ] world ] my beloved [ [ ! ', 1, false) }
    )
end

function this:testBackwardBackwardSplitBrackets()
    this:assertDeepEquals(
        { ' hello', ']', ' world ] my beloved [ ', '[', '! ' },
        { Unicode.splitBrackets(' hello ] world ] my beloved [ [ ! ', 1, true) }
    )
end

function this:testBackwardForwardSplitBrackets()
    this:assertDeepEquals(
        { ' hello [ world [ my beloved ] ] ! ', '', '', '', '' },
        { Unicode.splitBrackets(' hello [ world [ my beloved ] ] ! ', 1, true) }
    )
end

function this:testForwardNoneNone()
    this:assertEquals(
        nil,
        Unicode.findOutsideBrackets('hello world!', ',', 1, false)
    )
end

function this:testBackwardNoneNone()
    this:assertEquals(
        nil,
        Unicode.findOutsideBrackets('hello world!', ',', 1, true)
    )
end

function this:testForwardNoneCommas()
    this:assertEquals(
        6,
        Unicode.findOutsideBrackets('hello, world!', ',', 1, false)
    )
end

function this:testBackwardNoneCommas()
    this:assertEquals(
        6,
        Unicode.findOutsideBrackets('hello, world!', ',', 1, true)
    )
end

function this:testForwardForwardNone()
    this:assertEquals(
        nil,
        Unicode.findOutsideBrackets('hello [world, my friend]!', ',', 1, false)
    )
end

function this:testForwardBackwardNone()
    this:assertEquals(
        13,
        Unicode.findOutsideBrackets('hello ]world, my friend[!', ',', 1, false)
    )
end

function this:testBackwardBackwardNone()
    this:assertEquals(
        nil,
        Unicode.findOutsideBrackets('hello ]world, my friend[!', ',', 1, true)
    )
end

function this:testBackwardForwardNone()
    this:assertEquals(
        13,
        Unicode.findOutsideBrackets('hello [world, my friend]!', ',', 1, true)
    )
end

function this:testForwardForwardCommas()
    this:assertEquals(
        30,
        Unicode.findOutsideBrackets('hello [world, my friend]! yes, it is time!', ',', 1, false)
    )
end

function this:testForwardBackwardCommas()
    this:assertEquals(
        13,
        Unicode.findOutsideBrackets('hello ]world, my friend[! yes, it is time!', ',', 1, false)
    )
end

function this:testBackwardBackwardCommas()
    this:assertEquals(
        30,
        Unicode.findOutsideBrackets('hello ]world, my friend[! yes, it is time!', ',', 1, true)
    )
end

function this:testBackwardForwardCommas()
    this:assertEquals(
        13,
        Unicode.findOutsideBrackets('hello [world, my friend]! yes, it is time!', ',', 1, true)
    )
end

function this:testNonemptySplitCommas()
    local split = {}
    for _, s in Unicode.splitCommas('hello world!') do
        table.insert(split, s)
    end
    this:assertDeepEquals(
        { 'hello world!' },
        split
    )
end

function this:testEmptySplitCommas()
    local split = {}
    for _, s in Unicode.splitCommas('') do
        table.insert(split, s)
    end
    this:assertDeepEquals(
        { '' },
        split
    )
end

function this:testMultipleSplitCommas()
    local split = {}
    for _, s in Unicode.splitCommas(' , hello , world , ') do
        table.insert(split, s)
    end
    this:assertDeepEquals(
        { '', 'hello', 'world', '' },
        split
    )
end

function this:testMultipleSplitCommasForwardForward()
    local split = {}
    for _, s in Unicode.splitCommas(' , hello (1, 2) , world , ', 1, false) do
        table.insert(split, s)
    end
    this:assertDeepEquals(
        { '', 'hello (1, 2)', 'world', '' },
        split
    )
end

function this:testMultipleSplitCommasForwardBackward()
    local split = {}
    for _, s in Unicode.splitCommas(' , hello )1, 2( , world , ', 1, false) do
        table.insert(split, s)
    end
    this:assertDeepEquals(
        { '', 'hello )1', '2(', 'world', '' },
        split
    )
end

function this:testMultipleSplitCommasBackwardBackward()
    local split = {}
    for _, s in Unicode.splitCommas(' , hello )1, 2( , world , ', 1, true) do
        table.insert(split, s)
    end
    this:assertDeepEquals(
        { '', 'hello )1, 2(', 'world', '' },
        split
    )
end

function this:testMultipleSplitCommasBackwardForward()
    local split = {}
    for _, s in Unicode.splitCommas(' , hello (1, 2) , world , ', 1, true) do
        table.insert(split, s)
    end
    this:assertDeepEquals(
        { '', 'hello (1', '2)', 'world', '' },
        split
    )
end

return this
