--- Module:Errors
--- by User:LtDk, licensed CC-BY-SA 4.0
--- A class for handling template errors.

local List = require('Module:List')
local yesNo = require('Module:YesNo')

-- allow missing config
local status, ErrorConfig = pcall(require, 'Module:Errors/config')
if not status then
    ErrorConfig = {}
end

local Errors = {}

--- @class ErrorArgs
--- @field isexample string?
--- @field noerror string?
--- @field expecterror string?

--- @class ErrorStrings
--- @field open string
--- @field close string
--- @field comma string
--- @field error string
--- @field colon string

--- @alias GetCategory fun(Item): mw.title

--- Context for error, taken from frame.
--- @class Errors
--- @field isExample    boolean     If true, working in example context.
--- @field noError      boolean     If true, suppress all errors.
--- @field expectError  ListLookup  Lookup table of errors and payloads to exclude from reporting.
--- @field errors       List        Current list of errors.
local ErrorsProto = {}
local ErrorsMeta = {}

--- Checks if a value is an error context.
--- @param value any
--- @return boolean
function Errors.isContext(value)
    return type(value) == 'table' and rawequal(getmetatable(value), ErrorsMeta)
end

--- Adds arguments from Errors to another table.
--- @param args { [string|number]: string }
--- @return { [string|number]: string }
function ErrorsProto:augmentArgs(args)
    local selfArgs = rawget(self, '__args')
    args.isexample = tostring(yesNo(args.isexample, false) or selfArgs.isexample)
    args.noerror = tostring(yesNo(args.noerror, false) or selfArgs.noerror)
    if args.expecterror == nil then
        args.expecterror = selfArgs.expecterror
    else
        args.expecterror = tostring(args.expecterror) .. ', ' .. selfArgs.expecterror
    end
    return args
end

local function defaultGetCategoryInner(error)
    return mw.title.new('Pages with ' .. error.value, 'Category')
end

local function defaultGetCategory(title)
    return defaultGetCategoryInner
end

local function defaultGetStrings(title)
    return {
        open = '(',
        close = ')',
        comma = ', ',
        error = 'error',
        colon = ': ',
    }
end

--- Creates a new error context from a frame.
--- @param frame        mw.frame                       parent frame
--- @return Errors
function Errors.new(frame)
    local isExample = yesNo(frame.args['isexample'], false)
    local noError = yesNo(frame.args['noerror'], false)
    local expectError = List.parse(
        frame.args['expecterror'] or '',
        tostring,
        List.stringWeight
    )
    return setmetatable(
        {
            __isExample = isExample,
            __noError = noError,
            __expectError = expectError:toLookup(),
            __rootCategory = (
                ErrorConfig.rootCategory or
                mw.title.new('Pages with errors', 'Category')
            ),
            __getCategory = ErrorConfig.getCategory or defaultGetCategory,
            __args = {
                isexample = tostring(isExample),
                noerror = tostring(noError),
                expecterror = frame.args['expecterror'],
            },
            __getStrings = ErrorConfig.getStrings or defaultGetStrings,
            __errors = List.new(),
        },
        ErrorsMeta
    )
end

function ErrorsMeta:__newindex(k, v)
    if k == 'isExample' or k == 'noError' then
        rawset(self, '__' .. k, not not v)
    elseif k == 'expectError' then
        error('cannot set expectError; modify lookup directly')
    elseif k == 'errors' then
        error('cannot set errors; modify list directly')
    else
        error(string.format('invalid key %s', k))
    end
end

function ErrorsMeta:__index(k)
    if k == 'isExample' or k == 'noError' or k == 'expectError' or k == 'errors' then
        return rawget(self, '__' .. k)
    else
        return ErrorsProto[k]
    end
end

--- Checks if context is expecting a particular error.
--- @param item string|Item
--- @return boolean
function ErrorsProto:isExpecting(item)
    if not List.isItem(item) then
        item = Item.new(item)
    end
    if not item.weight or item.weight == '' then
        return List.lookupHas(self.expectError, item.value)
    else
        return self.expectError[item.value][item.weight]
    end
end

--- Adds an error to a context.
--- @param item string|Item
function ErrorsProto:insert(item)
    if not self.noError and not self:isExpecting(item) then
        self.errors:insert(item)
    end
end

--- Calls a template in the current context.
--- @param obj { template: mw.title|string, args: { [string|number]: string } }
function ErrorsProto:expandTemplate(obj)
    self:augmentArgs(obj.args)
    return rawget(self, '__frame'):expandTemplate(obj)
end

--- Outputs all the errors in context.
function ErrorsMeta:__tostring()
    local title = mw.title.getCurrentTitle()
    local strings = rawget(self, '__getStrings')(title)
    local getCategory = rawget(self, '__getCategory')(title)
    local rootCategory = rawget(self, '__rootCategory')
    local ret = ''
    if self.errors then
        if self.isExample then
            ret = '&#32;<strong class="error">' .. strings.open
            ret = ret .. strings.error .. strings.colon
            local first = true
            for _, error in pairs(self.errors) do
                if first then
                    first = false
                else
                    ret = ret .. strings.comma
                end
                ret = ret .. '[[:' .. getCategory(error).fullText .. '|'
                ret = ret .. error.value
                if error.weight ~= '' then
                    ret = ret .. strings.colon .. error.weight
                end
                ret = ret .. ']]'
            end
            ret = ret .. strings.close .. '</strong>'
        else
            local output = {}
            for _, error in pairs(self.errors) do
                local logged = 'error: ' .. error.value
                if error.weight and error.weight ~= '' then
                    logged = logged .. ' (' .. error.weight .. ')'
                end
                mw.log(logged)
                if not output[error.value] then
                    output[error.value] = true
                    ret = ret .. '[[' .. getCategory(error).fullText .. ']]'
                end
            end
            ret = ret .. '[[' .. rootCategory.fullText .. ']]'
        end
    end
    return ret
end

--- Implementation of {{error}}.
function Errors.errorTemplate(frame)
    frame = frame:getParent()
    local errors = Errors.new(frame)
    errors:insert(List.Item.new(frame.args[1], frame.args[2]))
    return errors
end

return Errors
