--- Module:List
--- by User:LtDk, licensed CC-BY-SA 4.0
--- A class for handling lists of things, potentially containing attached weights.

local Unicode = require('Module:Unicode')

local Item = {}

--- List item.
--- @class Item
--- @field value any    value for the item
--- @field weight any?  weight for the item
local ItemProto = {}
local ItemMeta = {}

--- Checks if a value is an Item.
--- @param value any
--- @return boolean
function Item.isItem(value)
    return type(value) == 'table' and rawequal(getmetatable(value), ItemMeta)
end

local List = { Item = Item, isItem = Item.isItem }

function ItemMeta.__eq(lhs, rhs)
    if not Item.isItem(rhs) then
        return false
    end
    return lhs.weight == rhs.weight and lhs.value == rhs.value
end

--- Common code used by __lt and __le.
--- Extracts the pairs that should be used for ordering.
local function item_pairs(lhs, rhs)
    local lcmp = rawget(lhs, '__byvalue')
    local rcmp = rawget(rhs, '__byvalue')
    if lcmp == nil then
        lcmp = rcmp
    end
    if rcmp == nil then
        rcmp = lcmp
    end
    if lcmp ~= rcmp then
        local left = 'weight'
        local right = 'value'
        if lcmp then
            left, right = right, left
        end
        error(string.format(
            'cannot compare Items; %q is by %s, %q is by %s',
            lhs, left,
            rhs, right
        ))
    end
    local l1, l2, r1, r2 = lhs.weight, lhs.value, rhs.weight, rhs.value
    -- note: default is by weight, so, we only switch if we're definitely by value
    if lcmp then
        l1, l2, r1, r2 = l2, l1, r2, r1
    end
    return l1, l2, r1, r2
end

--- Custom less-than that puts nil after everything.
local function nil_lt(lhs, rhs)
    if lhs == nil then
        return false
    end
    if rhs == nil then
        return true
    end
    return lhs < rhs
end

--- Custom less-than-or-equal that puts nil after everything.
local function nil_le(lhs, rhs)
    if lhs == nil then
        return false
    end
    if rhs == nil then
        return true
    end
    return lhs <= rhs
end

--- Whether items are compared by the value or weight first depends on the item.
function ItemMeta.__lt(lhs, rhs)
    if not Item.isItem(rhs) then
        error('cannot compare Item with non-Item')
    end
    local l1, l2, r1, r2 = item_pairs(lhs, rhs)
    if l1 ~= r1 then
        return nil_lt(l1, r1)
    end
    return nil_lt(l2, r2)
end

--- Whether items are compared by the value or weight first depends on the item.
function ItemMeta.__le(lhs, rhs)
    if not Item.isItem(rhs) then
        error('cannot compare Item with non-Item')
    end
    local l1, l2, r1, r2 = item_pairs(lhs, rhs)
    if l1 ~= r1 then
        -- it doesn't matter if we do lt or le, but we do le anyway for consistency
        return nil_le(l1, r1)
    end
    return nil_le(l2, r2)
end

function ItemMeta:__newindex(k, v) --
    if k == 'value' then
        -- value cannot be nil
        if v == nil then
            error(string.format('value cannot be nil'))
        else
            ItemMeta.__validatevalue(f, v)
        end
    elseif k == 'weight' then
        -- it's always okay to unset the weight
        if v == nil then
            rawset(f, '__weight', v)
        else
            -- but if we set the weight, validate it
            ItemMeta.__validateweight(self, v)
        end
    else
        error(string.format('invalid key %s', k))
    end
end

--- We rename the actual keys to ensure newindex always fires.
function ItemMeta:__index(k)
    if k == 'value' or k == 'weight' then
        return rawget(self, '__' .. k)
    else
        return ItemProto[k]
    end
end

--- Forces validation of value.
--- @param self Item
--- @param v any?
function ItemMeta:__validatevalue(v)
    local validate = rawget(self, '__validatevalue')
    local value = v
    if value == nil then
        value = rawget(self, '__value')
    end
    local orig = value
    if validate ~= nil then
        value = validate(value)
        if value == nil then
            error(string.format('invalid value for item: %q', tostring(orig)))
        end
    end
    rawset(self, '__value', value)
end

--- Forces validation of weight.
--- @param self Item
--- @param w any?
function ItemMeta:__validateweight(w)
    local value = rawget(self, '__value')
    local validate = rawget(self, '__validateweight')
    local weight = w
    if weight == nil then
        weight = rawget(self, '__weight')
        if weight == nil then
            return
        end
    end
    local orig = weight
    if validate ~= nil then
        weight = validate(value, weight)
        if weight == nil then
            error(string.format('invalid weight for item %q: %q', tostring(value), tostring(orig)))
        end
    end
    rawset(self, '__weight', weight)
end

--- Equivalent to formatAfter(' (', ')'). This is intended for debugging only.
function ItemMeta:__tostring()
    return self:formatAfter(' (', ')', ', ')
end

--- Formats an item using provided brackets after the value.
--- Also passes the formatting to the weight if it is an item.
--- @param open  string   open bracket
--- @param close string   close bracket
--- @param comma string?  comma separator; passed to nested lists
--- @return string
function ItemProto:formatAfter(open, close, comma)
    if self.weight ~= nil then
        local weight
        if List.isList(self.weight) then
            weight = self.weight:formatAfter(open, close, comma)
        else
            weight = tostring(self.weight)
        end
        return string.format('%s%s%s%s', tostring(self.value), open, weight, close)
    else
        return tostring(self.value)
    end
end

--- Formats an item using provided brackets before the value.
--- Also passes the formatting to the weight if it is an item.
--- @param open  string   open bracket
--- @param close string   close bracket
--- @param comma string?  comma separator; passed to nested lists
--- @return string
function ItemProto:formatBefore(open, close, comma)
    if self.weight ~= nil then
        local weight
        if List.isList(self.weight) then
            weight = self.weight:formatBefore(open, close, comma)
        else
            weight = tostring(self.weight)
        end
        return string.format('%s%s%s%s', open, weight, close, tostring(self.value))
    else
        return tostring(self.value)
    end
end

--- Nested validator. Done as a table to allow parent parser to pass in arguments.
--- @class Nested
--- @field rev boolean?
--- @field alt boolean?
--- @overload fun(value: any, weight?: any): any?

--- @alias ValidateValue fun(value: any): any?
--- @alias ValidateWeight (fun(value: any, weight?: any): any?)|Nested

--- Creates a new item with the given value and weight.
--- @param value  any   value of the item
--- @param weight any?  weight of the item
--- @param validateValue ValidateValue?
--- @param validateWeight ValidateWeight?
--- @param byValue boolean?
function Item.new(value, weight, validateValue, validateWeight, byValue)
    local item = setmetatable(
        {
            __value = value,
            __weight = weight,
            __validatevalue = validateValue,
            __validateweight = validateWeight,
            __byvalue = byValue,
        },
        ItemMeta
    )
    -- force validation
    ItemMeta.__validatevalue(item, value)
    ItemMeta.__validateweight(item, weight)
    return item
end

--- List of items.
--- @class List
local ListProto = {}
local ListMeta = { __index = ListProto }

--- Checks if a value is a List.
--- @param value any
--- @return boolean
function List.isList(value)
    return type(value) == 'table' and rawequal(getmetatable(value), ListMeta)
end

--- Iterates over the items in the list.
--- @return fun(items: { [integer]: Item }, integer?): integer?, Item?
--- @return { [integer]: Item }
--- @return nil
function ListMeta:__ipairs()
    return function (items, idx)
        idx = idx or 0
        if idx < #items then
            return idx + 1, items[idx + 1]
        end
    end, rawget(self, '__items'), nil
end

--- Iterates over the items in the list.
--- @return fun(items: { [integer]: Item }, integer?): integer?, Item?
--- @return { [integer]: Item }
--- @return nil
function ListMeta:__pairs()
    return ipairs(self)
end

--- Returns the length of the list.
--- @return integer
function ListMeta:__len()
    return #rawget(self, '__items')
end

--- Iterates over the items in the list.
--- @return fun(items: { [integer]: Item }, integer?): integer?, Item?
--- @return { [integer]: Item }
--- @return nil
function ListProto:items()
    return ipairs(self)
end

--- Inserts an item on the list.
--- Accepts an unweighted string, an Item, or a simple table-based list.
--- @param value any|Item|{ [1]: any, [2]: any? }  value of the item
function ListProto:insert(value)
    if not Item.isItem(value) then
        if type(value) == 'table' and (#value == 2 or #value == 1) then
            value = Item.new(value[1], value[2])
        else
            value = Item.new(value)
        end
    end

    -- enforce list properties
    rawset(value --[[@as Item]], '__validatevalue',  rawget(self, '__validatevalue'))
    rawset(value --[[@as Item]], '__validateweight', rawget(self, '__validateweight'))
    rawset(value --[[@as Item]], '__byvalue',        rawget(self, '__byvalue'))

    -- force validation
    ItemMeta.__validatevalue(value --[[@as Item]])
    ItemMeta.__validateweight(value --[[@as Item]])

    local items = rawget(self, '__items')
    table.insert(items, value)
end

--- Appends a list of items to the list.
--- @param list List|(string|Item)[]?
function ListProto:append(list)
    if list ~= nil then
        for _, item in ipairs(list) do
            self:insert(item)
        end
    end
end

--- Sorts the list.
function ListProto:sort()
    local items = rawget(self, '__items')
    table.sort(items)
end

--- Gets the length of the list.
--- @return integer
function ListProto:len()
    return getmetatable(self).__len(self)
end

function ListMeta.__eq(lhs, rhs)
    if not List.isList(rhs) then
        return false
    end
    if #lhs ~= #rhs then
        return false
    end
    for idx, litem in ipairs(lhs) do
        local ritem = rhs[idx]
        if litem ~= ritem then
            return false
        end
    end
    return true
end

--- Lists are compared lexicographically, with non-lists being sorted after lists.
function ListMeta.__lt(lhs, rhs)
    if not List.isList(rhs) then
        return false
    end
    for idx, litem in ipairs(lhs) do
        local ritem = rhs[idx]
        if litem ~= ritem then
            return nil_lt(litem, ritem)
        end
    end

    -- at this point, they are equal in prefix, so, must differ in length
    return #lhs < #rhs
end

--- Lists are compared lexicographically, with non-lists being sorted after lists.
function ListMeta.__le(lhs, rhs)
    if not List.isList(rhs) then
        return false
    end
    for idx, litem in ipairs(lhs) do
        local ritem = rhs[idx]
        if litem ~= ritem then
            return nil_le(litem, ritem)
        end
    end

    -- at this point, they are equal in prefix, which is enough
    return true
end

function ListMeta:__newindex(k, v)
    error('read-only; use methods instead')
end

--- Items are not formatted with language-aware brackets. This is intended for debugging only.
function ListMeta:__tostring()
    return self:formatAfter(' (', ')', ', ')
end

--- Formats items with the given separator and brackets, with weights after values.
--- @param open  string  open bracket
--- @param close string  close bracket
--- @param comma string  comma separator
--- @return string
function ListProto:formatAfter(open, close, comma)
    local ret = ''
    local first = true
    for _, item in ipairs(self) do
        if first then
            first = false
        else
            ret = ret .. comma
        end
        ret = ret .. item:formatAfter(open, close, comma)
    end
    return ret
end

--- Formats items with the given separator and brackets, with weights before values.
--- @param open  string  open bracket
--- @param close string  close bracket
--- @param comma string  comma separator
--- @return string
function ListProto:formatBefore(open, close, comma)
    local ret = ''
    local first = true
    for _, item in ipairs(self) do
        if first then
            first = false
        else
            ret = ret .. comma
        end
        ret = ret .. item:formatBefore(open, close, comma)
    end
    return ret
end

--- @alias ListLookupItem { [any]?: true }
local LookupItemMeta = {}

--- Normally, nil isn't a valid key, but we can get around this by giving it a special key.
local NilKey = {}

--- To aid with GC, lookup tables store their items in two tables:
--- 1. The first table, at key 1, weakly stores the keys. This is what gets used by __index.
--- 2. The second table, at key 2, strongly stores the keys. Items will get placed here if they're
---    nonempty, to ensure they don't get garbage-collected.
--- To ensure lookup items can actually access the parent to do this, they store a copy of the
--- strong storage nested in a key whose values are weak.
local ParentKey = {}

--- This key just stores the length of an item.
local LenKey = {}

--- Helper method to change length of lookup item.
function LookupItemMeta:__addLen(l)
    local len = rawget(self, LenKey)
    rawset(self, LenKey, len + l)

    if len ~= 0 and len + l == 0 then
        -- if we're emptying the item, remove from strong storage
        rawset(rawget(self, ParentKey), self, nil)
    elseif len == 0 and l ~= 0 then
        -- if we're making the item nonempty, add to strong storage
        rawset(rawget(self, ParentKey), self, true)
    end
end

function LookupItemMeta:__newindex(k, v)
    if v then
        v = true
    else
        v = nil
    end
    if k == nil then
        k = NilKey
    end
    local addLen = 0
    if rawget(self, k) then
        if not v then
            addLen = -1
        end
    else
        if v then
            addLen = 1
        end
    end
    LookupItemMeta.__addLen(self, addLen)
    return rawset(self, k, v)
end

function LookupItemMeta:__index(k)
    if k == nil then
        k = NilKey
    end
    return rawget(self, k) ~= nil
end

function LookupItemMeta:__pairs()
    return function (items, key)
        -- careful: convert nil to NilKey first
        if key == nil then
            key = NilKey
        end
        -- careful: convert StartKey to nil second
        if rawequal(key, StartKey) then
            key = nil
        end
        key = next(items, key)
        local val = rawget(items, key)
        -- finally: convert NilKey back to nil
        if rawequal(key, NilKey) then
            key = nil
        end
        return key, val
    end, self, StartKey
end

--- @alias ListLookup { [any]: ListLookupItem }
local LookupMeta = {}

function LookupMeta:__index(k)
    if k == nil then
        k = NilKey
    end
    local weakStorage = rawget(self, 1)
    local item = rawget(weakStorage, k)
    if item == nil then
        local strongStorage = rawget(self, 2)
        item = setmetatable(
            {
                [ParentKey] = setmetatable({ strongStorage }, { __mode = 'v' }),
                [LenKey] = 0,
            },
            LookupItemMeta
        )
        rawset(weakStorage, k, item)
    end
    return item
end

--- Since we already use nil to represent a real key, use a sentinel start key instead.
local StartKey = {}

function LookupMeta:__pairs()
    -- same as lookup items, but go through weak storage first
    return LookupItemMeta.__pairs(rawget(self, 1))
end

function LookupMeta:__newindex(k)
    error('read-only; can only assign to inner lookup tables')
end

--- Converts a list into a lookup table. Access items by value, then by weight.
--- @return ListLookup
function ListProto:toLookup()
    local lookup = setmetatable(
        {
            --- weak storage
            setmetatable({}, { __mode = 'v' }),
            -- strong storage
            {},
        },
        LookupMeta
    )
    for _, item in pairs(self) do
        local value = item.value
        local weight = item.weight
        if weight == nil then
            weight = NilKey
        end
        lookup[value][weight] = true
    end
    return lookup
end

--- Converts a lookup table back into a list.
--- @param lookup ListLookup
--- @param validateValue ValidateValue?
--- @param validateWeight ValidateWeight?
--- @param byValue boolean?
--- @return List
function List.fromLookup(lookup, validateValue, validateWeight, byValue)
    local l = List.new(nil, validateValue, validateWeight, byValue)
    for value, item in pairs(lookup) do
        for weight in pairs(item) do
            l:insert(Item.new(value, weight))
        end
    end
    return l
end

--- Checks whether a lookup item has any weights attached.
function List.lookupHas(lookup, value)
    return rawget(lookup[value], LenKey) > 0
end

--- Since nil can't be a key, this is used for empty weights.
List.NilWeight = NilKey

--- Creates a new List, potentially from another list.
--- If passed a function, this function will validate the weights for the list, returning nil or
--- otherwise failing on error.
--- @param list List|any[]?
--- @param validateValue ValidateValue?
--- @param validateWeight ValidateWeight?
--- @param byValue boolean?
--- @return List
function List.new(list, validateValue, validateWeight, byValue)
    local l = {
        __items = {},
        __validatevalue = validateValue,
        __validateweight = validateWeight,
        __byvalue = byValue,
    }
    setmetatable(l, ListMeta)
    l:append(list)
    return l
end

--- Creates a new List, sorted by value.
--- @param list List|any[]?
--- @param validateValue ValidateValue?
--- @param validateWeight ValidateWeight?
--- @return List
function List.byValue(list, validateValue, validateWeight)
    return List.new(list, validateValue, validateWeight, true)
end

--- Creates a new List, sorted by weight.
--- @param list List|any[]?
--- @param validateValue ValidateValue?
--- @param validateWeight ValidateWeight?
--- @return List
function List.byWeight(list, validateValue, validateWeight)
    return List.new(list, validateValue, validateWeight, false)
end

--- Parses a list from a string, treating commas as list separators
--- and converting brackets into weights.
--- @param list  string    string to parse into a list
--- @param validateValue ValidateValue?
--- @param validateWeight ValidateWeight?
--- @param byValue  boolean?
--- @param rev   boolean?  if true, expects the brackets to occur in reversed order
--- @param alt   boolean?  if true, expects the brackets to occur before the unbracketed text
--- @return List
function List.parse(list, validateValue, validateWeight, byValue, rev, alt)
    -- these help out the nested parser
    if type(validateWeight) == 'table' then
        if rev == nil then
            rev = validateWeight.rev
        else
            validateWeight.rev = rev
        end
        if alt == nil then
            alt = validateWeight.alt
        else
            validateWeight.alt = alt
        end
    end

    local l = List.new(
        nil,
        validateValue,
        validateWeight,
        byValue
    )
    for _, item in Unicode.splitCommas(list, 1, rev) do
        local value, weight, open, _ = Unicode.parseBrackets(item, nil, rev, alt)
        if value == nil then
            error(string.format('unable to parse list item: %q', item))
        end
        if open == '' then
            weight = nil
        end
        l:insert(Item.new(value, weight))
    end
    return l
end

--- Parses a list from a string, treating commas as list separators,
--- converting brackets into weights, and sorting by value.
--- @param list  string    string to parse into a list
--- @param validateValue ValidateValue?
--- @param validateWeight ValidateWeight?
--- @param rev   boolean?  if true, expects the brackets to occur in reversed order
--- @param alt   boolean?  if true, expects the brackets to occur before the unbracketed text
--- @return List
function List.byValueParsed(list, validateValue, validateWeight, rev, alt)
    return List.parse(list, validateValue, validateWeight, true, rev, alt)
end

--- Parses a list from a string, treating commas as list separators,
--- converting brackets into weights, and sorting by weight.
--- @param list  string    string to parse into a list
--- @param validateValue ValidateValue?
--- @param validateWeight ValidateWeight?
--- @param rev   boolean?  if true, expects the brackets to occur in reversed order
--- @param alt   boolean?  if true, expects the brackets to occur before the unbracketed text
--- @return List
function List.byWeightParsed(list, validateValue, validateWeight, rev, alt)
    return List.parse(list, validateValue, validateWeight, false, rev, alt)
end

--- Validation function that will try to parse a nested list if it fails.
--- @param validateValue ValidateValue?
--- @param validateWeight ValidateWeight
--- @return Nested
function List.nested(validateValue, validateWeight)
    --- @type Nested
    local parser = {}
    local meta = {}

    --- The actual validation function.
    --- @param self Nested
    --- @param value any
    --- @param weight any?
    --- @return any?
    function meta:__call(value, weight)
        local simple = validateWeight(value, weight)
        if simple ~= nil then
            return simple
        end
        return List.parse(
            weight,
            validateValue,
            self,
            nil,
            parser.rev,
            parser.alt
        )
    end

    setmetatable(parser, meta)
    return parser
end

--- Validation function wrapper that will ignore the value and only validate the weight.
--- @param validate fun(weight: any): any?
--- @return ValidateWeight
function List.weightOnly(validate)
    return function (_, weight)
        return validate(weight)
    end
end

--- Shorthand for List.weightOnly(tonumber).
--- @type ValidateWeight
List.numericWeight = List.weightOnly(tonumber)

--- Shorthand for List.weightOnly(tostring).
--- @type ValidateWeight
List.stringWeight = List.weightOnly(tostring)

return List
