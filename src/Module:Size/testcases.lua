--- Module:Size/testcases
--- by User:LtDk, licensed CC-BY-SA 4.0
--- Tests for Size module.

local this = require('Module:ScribuntoUnit'):new()
local Size = require('Module:Size')

function this:testIsSize()
    self:assertTrue(Size.isSize(Size.new(1, 1)))
end

function this:testIsWidthSize()
    self:assertTrue(Size.isSize(Size.new(1, nil)))
end

function this:testIsHeightSize()
    self:assertTrue(Size.isSize(Size.new(nil, 1)))
end

function this:testEmptySize()
    self:assertThrows(
        function ()
            local _ = Size.new(nil, nil)
        end,
        'Size cannot have both nil width and nil height'
    )
end

function this:testParseEmpty()
    self:assertThrows(
        function ()
            local _ = Size.parse(' px ')
        end,
        '" px " is not a valid size'
    )
end

function this:testParseWidth()
    self:assertEquals(
        Size.new(400, nil),
        Size.parse(' 400 px ')
    )
end

function this:testParseHeight()
    self:assertEquals(
        Size.new(nil, 400),
        Size.parse(' x 400 px ')
    )
end

function this:testParseBoth()
    self:assertEquals(
        Size.new(40, 400),
        Size.parse(' 40 x 400 px ')
    )
end

return this
