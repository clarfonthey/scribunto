== Details ==

The StringSet module offers a Lua class that makes it easier to work with sets of strings. It's intended to be used by other Lua modules instead of templates.

It has no dependencies and exports a <code>StringSet</code> "class" you can use directly. It provides annotations documenting the various functions that are understood by Lua Language Server.
