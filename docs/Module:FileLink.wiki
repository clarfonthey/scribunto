== Details ==

The FileLink module offers a Lua class that makes it easier to work with links to files, as well as some functions to create file-linking templates. It uses [[Module:Size]], [[Module:StringSet]], and [[Module:Time]] internally to do this.

Unlike ordinary file links, this module will ignore key arguments that aren't used for files, making it useful in templates which create links to files but may be configured with other arguments. Although it will fail if there are more than two positional arguments (a file and a caption), the methods specifically for use in templates have some ways of getting around this.

This page will only document the functionality specific to templates, since those looking to understand how to use it in modules can just read the code directly.

== Usage ==

=== <code>fromFrame</code> ===

The <code>fromFrame</code> function simply creates a file link directly from the arguments passed to it via an <code><nowiki>{{#invoke:...}}</nowiki></code> call. So, for example, <code><nowiki>{{#invoke:FileLink|fromFrame|MyFile|...}}</nowiki></code> will be roughly equivalent to <code><nowiki>[[File:MyFile|...]]</nowiki></code>, although unrecognized key arguments will be ignored as described earlier.

This function mostly exists to test out how the FileLink module works, and probably isn't very useful in practice. You can experiment with it for yourself on a user page.

=== <code>fromParent</code> ===

This is the intended way to use FileLink in templates, and it's designed to help create templates which have custom options added to file links by default. It has access to both the arguments passed directly to it via <code><nowiki>{{#invoke:...}}</nowiki></code> ''and'' those passed to the parent template as well, making it quite powerful. Essentially, the invocation arguments are used to set default options for the file link while the parent's arguments are then used to construct the actual link. You can think of this as a way of converting a template into a special <code>fromFrame</code> call with custom defaults, where those defaults are included in the template itself.

The original code for this module (before it was rewritten), for example, was to create a pixelated image template which always applied a <code>pixelated</code> class to images that got passed to it. This template can be represented with the following code:

<pre>{{#invoke:FileLink|fromParent|class=pixelated}}</pre>

However, because you can use wikitext to control the arguments provided here, you can actually do more than just set options. For example, the old Pixelated module allowed an <code>unpixelated=1</code> argument that would ''remove'' the pixelated class, so that you could opt out of pixelation instead of opting in by providing the class. You can accomplish this via:

<pre>{{#invoke:FileLink|fromParent|class={{#if:{{{unpixelated|}}}||pixelated}}}}</pre>
